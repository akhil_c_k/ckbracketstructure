//
//  ViewController.swift
//  CKBracketUsingPanGesture
//
//  Copyright © 2017 Akhil CK. All rights reserved.
//

import UIKit

class CKBracketViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    
    let contentView = UIView()
    var numberOfRounds:Int  = 3
    var matchesInSection:[Int] = [4,2,1]
    
    var tableViewW = CGFloat(UIScreen.main.bounds.width * 0.8)
    var tableViewY = CGFloat(20)
    var tableViewX = CGFloat(0)
    var tableViewH = CGFloat(UIScreen.main.bounds.height)
    
    var translationX:CGFloat = 0{
        didSet{
            for table in tableViewArray{
                table.frame.origin.y = 0
                table.beginUpdates()
                table.endUpdates()
            }
        }
    }
    
    var mainW = UIScreen.main.bounds.width
    
    var tableViewArray: [UITableView] = []

    var currentPage = 0{
        didSet{
            for table in tableViewArray{
                table.frame.origin.y = 0
                table.beginUpdates()
                table.endUpdates()
            }
        }
    }
    
    var didPanEnd = false
    var didViewLoad = false
    
    
    let cellHeight:CGFloat = 180
    let shrinkedCellHeight:CGFloat = 100
    
    

    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        contentView.frame = CGRect(x: 0, y: 0, width: CGFloat(numberOfRounds) * mainW, height: UIScreen.main.bounds.height)
        self.view.addSubview(contentView)
        
        
        addRounds()

        let gest = UIPanGestureRecognizer(target: self, action: #selector(self.panGestureHandler(panGesture:)))
        gest.minimumNumberOfTouches = 1
        self.contentView.addGestureRecognizer(gest)
        
        didViewLoad = true
        
        
    }

    
    func addRounds(){
        
        for _ in 0..<numberOfRounds{
            
            let table = UITableView()
            table.frame = CGRect(x: tableViewX, y: tableViewY, width: tableViewW, height: tableViewH)
            tableViewX += tableViewW
            
            table.delegate = self
            table.dataSource = self
            table.register(UINib(nibName: "CKBracketCell", bundle: nil), forCellReuseIdentifier: "CKBracketCell")
            table.separatorStyle = .none
            table.showsVerticalScrollIndicator = false

            
            tableViewArray.append(table)
            self.contentView.addSubview(table)
            table.reloadData()

            
        }
        
        
        
        
    }
    
    //MARK: TableViewDataSource and Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
    
        switch tableView {
            
        case tableViewArray[0]:
            return matchesInSection[0]
            
        case tableViewArray[1]:
            return matchesInSection[1]
            
        case tableViewArray[2]:
            return matchesInSection[2]
        case tableViewArray[3]:
            return matchesInSection[3]
        case tableViewArray[4]:
            return matchesInSection[4]
            
        default:
            break
            
        }
        
        return 0
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CKBracketCell", for: indexPath) as! CKBracketCell
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
            
            switch tableView {
                
            case tableViewArray[0]:
                
                return currentPage == 0 ? cellHeight : shrinkedCellHeight
                
            case tableViewArray[1]:
                
                if currentPage == 0{
                    
                    return cellHeight * 2
                    
                }
                else if currentPage == 1{
                    
                    return cellHeight
                    
                }else if currentPage == 2{
                    
                    return shrinkedCellHeight
                }
                
                
            case tableViewArray[2]:
                
                if currentPage == 0{
                    
                    return cellHeight * 3
                    
                }else
                    if currentPage == 1{
                        
                        return cellHeight * 2
                        
                    }else if currentPage == 2{
                        
                        return cellHeight
                }
                
            default:
                break
                
            }
            
        

        
        return 0
    }
    
    
    // MARK: PanGesture
    
    func panGestureHandler(panGesture recognizer: UIPanGestureRecognizer) {
        
        didViewLoad = false
        
        let translation = recognizer.translation(in: self.view)
        if  recognizer.state == .began ||  recognizer.state == .changed{
            
            didPanEnd = false
            print("gestx:\(self.contentView.frame.origin.x)")
            print("gesty:\(self.contentView.frame.origin.y)")
            
            
            
                recognizer.view!.center.x = recognizer.view!.center.x + translation.x
                recognizer.setTranslation(CGPoint.zero, in: self.view)
            
            translationX = translation.x
            //transformCell()
            
            
        }else if recognizer.state == .ended{
            
            didPanEnd = true

            let scrollLimit = mainW * 0.5
            
            switch self.contentView.frame.origin.x {
                
            case -scrollLimit ... 1000:
                UIView.animate(withDuration: 0.25, animations: {
                    recognizer.view!.frame.origin.x = CGFloat(0)
                    self.currentPage = 0
                })
                
            case -scrollLimit*2 ... -scrollLimit:
                UIView.animate(withDuration: 0.25, animations: {
                    recognizer.view!.frame.origin.x =  CGFloat(0 - self.tableViewW)
                    self.currentPage = 1
                })
                
            case -scrollLimit*3 ... -scrollLimit*2 :
                UIView.animate(withDuration: 0.25, animations: {
                    recognizer.view!.frame.origin.x = CGFloat(0 - (self.tableViewW  * 2))
                    self.currentPage = 2
                })
                

            default:
                UIView.animate(withDuration: 0.25, animations: {
                    recognizer.view!.frame.origin.x = CGFloat(0 - (self.tableViewW * 2))
                    self.currentPage = 2
                })
                

                break
            }
            

            
            
            //recognizer.setTranslation(CGPoint.zero, in: self.view)
            
            

        }
       
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        
        
        
        switch scrollView {
            
        case tableViewArray[0]:
            
            tableViewArray[currentPage+1].frame.origin.y = -scrollView.contentOffset.y
            
        case tableViewArray[1]:
            
            if currentPage == 0{
                
                tableViewArray[currentPage].frame.origin.y = -scrollView.contentOffset.y
                
            }else{
                
                tableViewArray[currentPage+1].frame.origin.y = -scrollView.contentOffset.y
                
            }
        default:
            break
            
        }
        
    }
    
    
    func transformCell(){
        
        
        
        for table in tableViewArray{
            for cell in (table.visibleCells) {
                
                var offsetX = translationX
                if offsetX < 0 {
                    offsetX *= -1
                }
                
                cell.transform = CGAffineTransform(scaleX: 1, y: 1)
                print("offsetX:\(offsetX)")
                if offsetX > 3 {
                    
                    let offsetPercentage = (offsetX - 3) / view.bounds.width
                    var scaleX = 1-offsetPercentage
                    
                    if scaleX < 0.8 {
                        scaleX = 0.8
                    }
                    cell.transform = CGAffineTransform(scaleX: scaleX, y: scaleX)
                }
            }
        }

    }

}



